package com.uvigo.watermelons.comedor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private ImageView icono;
    private EditText dni;
    private Button envio, comida, comidaf;
    private String identificador;
    private static final String IP = "77.27.223.147";
    private static final int PORT = 2045;
    static GlobalParameters globalParameters;
    boolean[] valores = {false, false};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        icono = (ImageView) findViewById(R.id.imageView);
        dni = (EditText) findViewById(R.id.editText);
        comida = (Button) findViewById(R.id.button_comida);
        comidaf = (Button) findViewById(R.id.button_comidasFuturas);
        envio = (Button) findViewById(R.id.boton_envio);

        globalParameters = (GlobalParameters) getApplicationContext();
        cargarDatos();

        dni.setText(globalParameters.getDni());

        comida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ComidasActivity.class);
                startActivity(intent);
            }
        });

        comidaf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, activity_futuras_comidas.class);
                startActivity(intent);
            }
        });


        envio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (dni.getText().toString().equals("")) {
                    dni.setError("Introduzca su DNI");
                } else {
                /*CONSTRUIMOS EL ALERT DIALOG CON DOS ELECCIONES*/
                    selectComida();


                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //android.app.ActionBar actionbar = getActionBar();
        //actionbar.setIcon(R.mipmap.iconprincipal);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.contacto:
                mostrarContacto();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        guardarDatos();
    }

    public void mostrarContacto() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        AlertDialog alerta = builder.create();
        alerta.setTitle("Contacto");
        alerta.setMessage("Teléfono:886111076\n Email:residencia-xuvenil-altamar@xunta.gal");
        alerta.show();
    }

    public void selectComida() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        final String[] opciones = {"Semana actual", "Semana siguiente"};
        // final boolean[] valores = {false, false};

        builder.setTitle("Enviar");
            /*which indica con un int el elemento del array opciones seleccionado,
            * el boolean indica si está seleccionado o no*/
        builder.setMultiChoiceItems(opciones, valores, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which, boolean isChecked) {

                /*ACTUALIZAMOS LOS VALORES PUESTOS A FALSE POR DEFECTO*/
                valores[which] = isChecked;

            }


        });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Toast.makeText(getApplicationContext(),
                        "Enviado", Toast.LENGTH_SHORT).show();

                identificador = dni.getText().toString();
                globalParameters.setDni(identificador);
                Calendar calendar = Calendar.getInstance();
                int j = calendar.get(Calendar.HOUR_OF_DAY);

                if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY)
                    valores[0] = false;

                    if (valores[0] == true && valores[1] == true) {
                     /*LAS DOS SEMANAS*/
                        new NetworkCommunication().execute("2");
                    } else if (valores[0] == true && valores[1] == false) {
                        /*SEMANA ACTUAL SOLA*/
                        new NetworkCommunication().execute("0");
                    } else if (valores[0] == false && valores[1] == true) {
                     /*SEMANA SIGUIENTE SOLA*/
                        new NetworkCommunication().execute("1");
                    } else {
                    /*NADA, OPERACIÓN CANCELADA*/
                    }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(getApplicationContext(),
                        "Cancelado", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void cargarDatos() {

        SharedPreferences preferences = getSharedPreferences("ComedorApp", Context.MODE_PRIVATE);
        globalParameters.setLuCo(preferences.getString("LCO", "L,X"));
        globalParameters.setMaCo(preferences.getString("MCO", "M,X"));
        globalParameters.setMiCo(preferences.getString("XCO", "X,X"));
        globalParameters.setJuCo(preferences.getString("JCO", "J,X"));
        globalParameters.setViCo(preferences.getString("VCO", "V,X"));

        globalParameters.setLuCe(preferences.getString("LCE", "L,X"));
        globalParameters.setMaCe(preferences.getString("MCE", "M,X"));
        globalParameters.setMiCe(preferences.getString("XCE", "X,X"));
        globalParameters.setJuCe(preferences.getString("JCE", "J,X"));
        globalParameters.setViCe(preferences.getString("VCE", "V,X"));
        globalParameters.setDni(preferences.getString("DNI", ""));

        globalParameters.setLuCof(preferences.getString("LCOF", "L,X"));
        globalParameters.setMaCof(preferences.getString("MCOF", "M,X"));
        globalParameters.setMiCof(preferences.getString("XCOF", "X,X"));
        globalParameters.setJuCof(preferences.getString("JCOF", "J,X"));
        globalParameters.setViCof(preferences.getString("VCOF", "V,X"));

        globalParameters.setLuCef(preferences.getString("LCEF", "L,X"));
        globalParameters.setMaCef(preferences.getString("MCEF", "M,X"));
        globalParameters.setMiCef(preferences.getString("XCEF", "X,X"));
        globalParameters.setJuCef(preferences.getString("JCEF", "J,X"));
        globalParameters.setViCef(preferences.getString("VCEF", "V,X"));
    }

    public void guardarDatos() {
        SharedPreferences preferences = getSharedPreferences("ComedorApp", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LCO", globalParameters.getLuCo());
        editor.putString("MCO", globalParameters.getMaCo());
        editor.putString("XCO", globalParameters.getMiCo());
        editor.putString("JCO", globalParameters.getJuCo());
        editor.putString("VCO", globalParameters.getViCo());

        editor.putString("LCE", globalParameters.getLuCe());
        editor.putString("MCE", globalParameters.getMaCe());
        editor.putString("XCE", globalParameters.getMiCe());
        editor.putString("JCE", globalParameters.getJuCe());
        editor.putString("VCE", globalParameters.getViCe());
        editor.putString("DNI", globalParameters.getDni());

        editor.putString("LCOF", globalParameters.getLuCof());
        editor.putString("MCOF", globalParameters.getMaCof());
        editor.putString("XCOF", globalParameters.getMiCof());
        editor.putString("JCOF", globalParameters.getJuCof());
        editor.putString("VCOF", globalParameters.getViCof());

        editor.putString("LCEF", globalParameters.getLuCef());
        editor.putString("MCEF", globalParameters.getMaCef());
        editor.putString("XCEF", globalParameters.getMiCef());
        editor.putString("JCEF", globalParameters.getJuCef());
        editor.putString("VCEF", globalParameters.getViCef());
        editor.commit();
    }

    public static void connecting(Socket socket, String string, int i) throws IOException {


        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.writeInt(i);
        out.writeUTF(string);
        out.flush();

    }


    public class NetworkCommunication extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {

            String string;
            try {
                Socket socket = new Socket(InetAddress.getByName(IP), PORT);
                DataInputStream in = new DataInputStream(socket.getInputStream());


                if (strings[0].equals("2")) {
                    /*ENVIAMOS EN EL FORMATO DOS SEMANAS*/
                    String semanaAct = globalParameters.getDni() + "-CO:" + globalParameters.getLuCo() + ":" + globalParameters.getMaCo() + ":" + globalParameters.getMiCo() + ":" + globalParameters.getJuCo() + ":"
                            + globalParameters.getViCo() + "-CE:" + globalParameters.getLuCe() + ":" + globalParameters.getMaCe() + ":" + globalParameters.getMiCe() + ":" + globalParameters.getJuCe()
                            + ":" + globalParameters.getViCe();
                    String semanaFut = globalParameters.getDni() + "-CO:" + globalParameters.getLuCof() + ":" + globalParameters.getMaCof() + ":" + globalParameters.getMiCof() + ":" + globalParameters.getJuCof() + ":"
                            + globalParameters.getViCof() + "-CE:" + globalParameters.getLuCef() + ":" + globalParameters.getMaCef() + ":" + globalParameters.getMiCef() + ":" + globalParameters.getJuCef()
                            + ":" + globalParameters.getViCef();
                    string = semanaAct + "*" + semanaFut;
                    MainActivity.connecting(socket, string, 2);
                } else if (strings[0].equals("1")) {
                    /*ENVIAMOS SEMANA FUTURA*/
                    string = globalParameters.getDni() + "-CO:" + globalParameters.getLuCof() + ":" + globalParameters.getMaCof() + ":" + globalParameters.getMiCof() + ":" + globalParameters.getJuCof() + ":"
                            + globalParameters.getViCof() + "-CE:" + globalParameters.getLuCef() + ":" + globalParameters.getMaCef() + ":" + globalParameters.getMiCef() + ":" + globalParameters.getJuCef()
                            + ":" + globalParameters.getViCef();
                    MainActivity.connecting(socket, string, 1);
                } else {
                    /*ENVIAMOS SEMANA ACTUAL*/
                    string = globalParameters.getDni() + "-CO:" + globalParameters.getLuCo() + ":" + globalParameters.getMaCo() + ":" + globalParameters.getMiCo() + ":" + globalParameters.getJuCo() + ":"
                            + globalParameters.getViCo() + "-CE:" + globalParameters.getLuCe() + ":" + globalParameters.getMaCe() + ":" + globalParameters.getMiCe() + ":" + globalParameters.getJuCe()
                            + ":" + globalParameters.getViCe();
                    MainActivity.connecting(socket, string, 0);
                }
                return in.readUTF();
            } catch (ConnectException ex) {
                return "OUT";
            } catch (IOException e) {
                e.printStackTrace();

            }
            /*Aqui solo llegamos si salta la excepción*/
            return "OUT";
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if (aVoid.equals("OK")) {
                 /*Es correcto el DNI*/
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog alerta = builder.create();
                alerta.setTitle("Atención!");
                alerta.setMessage("Sus horarios ya han sido enviados al sistema correctamente!");
                alerta.show();

            } else if (aVoid.equals("OUT")) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog alerta = builder.create();
                alerta.setTitle("Atención!");
                alerta.setMessage("El sistema no se encuentra disponible en este momento.");
                alerta.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                AlertDialog alerta = builder.create();
                alerta.setTitle("Atención!");
                alerta.setMessage("No existe tal DNI en la base de datos: \n1.-Compruebe que sea correcto. \n2.-En caso contrario póngase en contacto con el administrador");
                alerta.show();
            }
            guardarDatos();
        }
    }


}
