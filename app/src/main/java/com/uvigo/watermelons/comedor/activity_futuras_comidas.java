package com.uvigo.watermelons.comedor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

public class activity_futuras_comidas extends AppCompatActivity {

    Spinner lCof, lCef, mCof, mCef, xCof, xCef, jCof, jCef, vCof, vCef;
    TextView lunes, martes, miercoles, jueves, viernes;
    private GlobalParameters globalParameters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comidas);

        lCof = (Spinner) findViewById(R.id.spinner1);
        lCef = (Spinner) findViewById(R.id.spinner2);
        mCof = (Spinner) findViewById(R.id.spinner3);
        mCef = (Spinner) findViewById(R.id.spinner4);
        xCof = (Spinner) findViewById(R.id.spinner5);
        xCef = (Spinner) findViewById(R.id.spinner6);
        jCof = (Spinner) findViewById(R.id.spinner7);
        jCef = (Spinner) findViewById(R.id.spinner8);
        vCof = (Spinner) findViewById(R.id.spinner9);
        vCef = (Spinner) findViewById(R.id.spinner10);

        ArrayList<Spinner> spinners = new ArrayList<>();

        final GlobalParameters globalParameters = (GlobalParameters) getApplicationContext();

        spinners.add(lCof);
        spinners.add(lCef);
        spinners.add(mCof);
        spinners.add(mCef);
        spinners.add(xCof);
        spinners.add(xCef);
        spinners.add(jCof);
        spinners.add(jCef);
        spinners.add(vCof);
        spinners.add(vCef);

        lunes = (TextView) findViewById(R.id.lunes);
        martes = (TextView) findViewById(R.id.martes);
        miercoles = (TextView) findViewById(R.id.miercoles);
        jueves = (TextView) findViewById(R.id.jueves);
        viernes = (TextView) findViewById(R.id.viernes);

        for (Spinner spinner : spinners) {
            ArrayAdapter<CharSequence> adaptador = ArrayAdapter.createFromResource(this, R.array.horarios_array, android.R.layout.simple_spinner_item);
            adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            spinner.setAdapter(adaptador);
        }

        /*AHORA COMPROBAMOS LA FECHA A LA QUE ESTAMOS PARA VER QUE BOTONES DEBEMOS HABILITAR*/

        lCof.setSelection(queNumero(globalParameters.getLuCof()));
        lCef.setSelection(queNumero(globalParameters.getLuCef()));
        mCof.setSelection(queNumero(globalParameters.getMaCof()));
        mCef.setSelection(queNumero(globalParameters.getMaCef()));
        xCof.setSelection(queNumero(globalParameters.getMiCof()));
        xCef.setSelection(queNumero(globalParameters.getMiCef()));
        jCof.setSelection(queNumero(globalParameters.getJuCof()));
        jCef.setSelection(queNumero(globalParameters.getJuCef()));
        vCof.setSelection(queNumero(globalParameters.getViCof()));
        vCef.setSelection(queNumero(globalParameters.getViCef()));


        Calendar calendar = Calendar.getInstance();


        /*AHORA CARGAMOS LA INFORMACIÓN GUARDADA DE LA ÚLTIMA VEZ QUE ENVIAMOS DATOS*/
        // cargarDatos();




        lCof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setLuCof("L,X");
                        break;
                    case 1:
                        globalParameters.setLuCof("L,N");
                        break;
                    case 2:
                        globalParameters.setLuCof("L,T");
                        break;
                    case 3:
                        globalParameters.setLuCof("L,P");
                        break;
                    case 4:
                        globalParameters.setViCof("L,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setLuCof("L,X");
            }
        });
        lCef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setLuCef("L,X");
                        break;
                    case 1:
                        globalParameters.setLuCef("L,N");
                        break;
                    case 2:
                        globalParameters.setLuCef("L,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setLuCef("L,X");
            }
        });
        mCof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMaCof("M,X");
                        break;
                    case 1:
                        globalParameters.setMaCof("M,N");
                        break;
                    case 2:
                        globalParameters.setMaCof("M,T");
                        break;
                    case 3:
                        globalParameters.setMaCof("M,P");
                        break;
                    case 4:
                        globalParameters.setViCof("M,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMaCof("M,X");
            }
        });
        mCef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMaCef("M,X");
                        break;
                    case 1:
                        globalParameters.setMaCef("M,N");
                        break;
                    case 2:
                        globalParameters.setMaCef("M,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMaCef("M,X");
            }
        });
        xCof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMiCof("X,X");
                        break;
                    case 1:
                        globalParameters.setMiCof("X,N");
                        break;
                    case 2:
                        globalParameters.setMiCof("X,T");
                        break;
                    case 3:
                        globalParameters.setMiCof("X,P");
                        break;
                    case 4:
                        globalParameters.setViCof("X,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMiCof("X,X");
            }
        });
        xCef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setMiCef("X,X");
                        break;
                    case 1:
                        globalParameters.setMiCef("X,N");
                        break;
                    case 2:
                        globalParameters.setMiCef("X,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setMiCef("X,X");
            }
        });
        jCof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setJuCof("J,X");
                        break;
                    case 1:
                        globalParameters.setJuCof("J,N");
                        break;
                    case 2:
                        globalParameters.setJuCof("J,T");
                        break;
                    case 3:
                        globalParameters.setJuCof("J,P");
                        break;
                    case 4:
                        globalParameters.setViCof("J,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setJuCof("J,X");
            }
        });
        jCef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setJuCef("J,X");
                        break;
                    case 1:
                        globalParameters.setJuCef("J,N");
                        break;
                    case 2:
                        globalParameters.setJuCef("J,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setJuCef("J,X");
            }
        });
        vCof.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setViCof("V,X");
                        break;
                    case 1:
                        globalParameters.setViCof("V,N");
                        break;
                    case 2:
                        globalParameters.setViCof("V,T");
                        break;
                    case 3:
                        globalParameters.setViCof("V,P");
                        break;
                    case 4:
                        globalParameters.setViCof("V,B");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setViCof("V,X");
            }
        });
        vCef.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        globalParameters.setViCef("V,X");
                        break;
                    case 1:
                        globalParameters.setViCef("V,N");
                        break;
                    case 2:
                        globalParameters.setViCef("V,T");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                globalParameters.setViCef("V,X");
            }
        });


    }

    public int queNumero(String string){

        switch (string.charAt(2)){
            case 'X':
                return 0;
            case 'N':
                return 1;
            case 'T':
                return 2;
            case 'P':
                return 3;
            case 'B':
                return 4;
        }
        return 0;
    }



}
